/** @format */

// I had the pleasure of working in Suris group study. yarn

import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    createnewTodo: "",
  };
  handleSubmit = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      let addedTodo = event.target.value;
      this.addATodo({ ...this.state.todos }, addedTodo);
      this.refs.createnewTodo.value = "";
    }
  };
  addATodo = (todoList, addedTodo) => {
    let newIndex = this.state.todos.length;
    const newItem = {
      userId: 1,
      id: newIndex + 1,
      title: addedTodo,
      completed: false,
    };
    this.setState((state) => ({
      todos: [...state.todos, newItem],
    }));
    this.setState({
      createnewTodo: "",
    });
  };
  deleteTodo = (id) => (event) => {
    const createnewTodoList = this.state.todos.slice();
    let index = createnewTodoList.findIndex((todo) => {
      if (todo.id === id) {
        return true;
      }
    });
    createnewTodoList.splice(index, 1);
    this.setState({
      todos: createnewTodoList,
    });
  };
  deleteAllCompletedTodos = () => {
    const createnewTodoList = this.state.todos.filter((todo) => {
      if (todo.completed) {
        return false;
      } else {
        return true;
      }
    });
    this.setState({
      todos: createnewTodoList,
    });
  };
  todoComplete = (id) => (event) => {
    const createnewTodoList = this.state.todos.slice();
    createnewTodoList.map((todo) => {
      if (todo.id === id) {
        if (todo.completed) {
          todo.completed = false;
        } else {
          todo.completed = true;
        }
      }
    });
    this.setState({
      todos: createnewTodoList,
    });
  };
  render() {
    return (
      <section className='todoapp'>
        <header className='header'>
          <h1>todos</h1>
          <input
            className='new-todo'
            placeholder='What needs to be done?'
            autoFocus
            onKeyPress={this.handleSubmit}
            ref='createnewTodo'
          />
        </header>
        <TodoList
          todos={this.state.todos}
          deleteTodo={this.deleteTodo}
          todoComplete={this.todoComplete}
        />
        <footer className='footer'>
          <span className='todo-count'>
            <strong>0</strong> item(s) left
          </span>
          <button
            className='clear-completed'
            onClick={this.deleteAllCompletedTodos}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li
        className={this.props.completed ? "completed" : ""}
        key={this.props.id}>
        <div className='view'>
          <input
            className='toggle'
            type='checkbox'
            checked={this.props.completed}
            onChange={this.props.todoComplete}
          />
          <label>{this.props.title}</label>
          <button className='destroy' onClick={this.props.deleteTodo} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className='main'>
        <ul className='todo-list'>
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              key={todo.id}
              deleteTodo={this.props.deleteTodo(todo.id)}
              todoComplete={this.props.todoComplete(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
